import { Component, OnInit, ViewEncapsulation } from '@angular/core';
declare var Stripe: any;

@Component({
  selector: 'app-create-subscription',
  templateUrl: './create-subscription.component.html',
  styleUrls: ['./create-subscription.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateSubscriptionComponent implements OnInit {
  PUBLISHABLE_KEY = "pk_test_QrgGFFIn2rjHnwgwvakXU0dn00FhK9IbmE";
  SUBSCRIPTION_BASIC_PRICE_ID = "price_1H7lWsAB8AQDDF5i7Bnobzq1";
  SUBSCRIPTION_PRO_PRICE_ID = "price_1H7lX6AB8AQDDF5iaBeXQrhg";

  constructor() { }

  ngOnInit(): void {
    this.initCheckout();
  }

  initCheckout() {
    var stripe = Stripe('pk_test_QrgGFFIn2rjHnwgwvakXU0dn00FhK9IbmE');
    var DOMAIN = 'http://localhost:4200';

    var handleResult = function (result) {
      if (result.error) {
        var displayError = document.getElementById("error-message");
        displayError.textContent = result.error.message;
      }
    };

    var redirectToCheckout = function (priceId) {
      // Make the call to Stripe.js to redirect to the checkout page
      // with the current quantity
      stripe
        .redirectToCheckout({
          lineItems: [{ price: priceId, quantity: 1 }],
          successUrl:
            DOMAIN + "/success-subscription?session_id={CHECKOUT_SESSION_ID}",
          cancelUrl: DOMAIN + "/subscription?session_id={CHECKOUT_SESSION_ID}",
          mode: "subscription",
        })
        .then(handleResult);
    };

    document
      .getElementById("basic-btn")
      .addEventListener("click", function (evt) {
        redirectToCheckout("price_1H7lWsAB8AQDDF5i7Bnobzq1");
      });

    document
      .getElementById("pro-btn")
      .addEventListener("click", function (evt) {
        redirectToCheckout("price_1H7lX6AB8AQDDF5iaBeXQrhg");
      });
  }

}
