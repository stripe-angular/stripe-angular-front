import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
declare var Stripe: any;
var userCardInfo = "";

@Component({
  selector: 'app-stripe-payment',
  templateUrl: './stripe-payment.component.html',
  styleUrls: ['./stripe-payment.component.scss']
})
export class StripePaymentComponent implements OnInit {
  paymentForm: FormGroup;
  userInfo: boolean = false;
  purchase = {
    items: [{ id: "xl-tshirt" }],
    id: 'test_ID-USERS',
  };

  constructor(private http: HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.paymentForm = this.formBuilder.group({
      firstname: '',
      lastname: '',
      terms: null
    });
    this.onValueChanges();
    this.initStripe();
  }

  onValueChanges(): void {
    this.paymentForm.valueChanges.subscribe(val => {
      val.firstname && val.lastname && val.terms ? this.userInfo = true : this.userInfo = false;
      this.setPurchaseInfo();
    })
  }

  setPurchaseInfo() {
    userCardInfo = this.paymentForm.get('firstname').value + ' ' + this.paymentForm.get('lastname').value;
  }

  initStripe() {
    var stripe = Stripe("pk_test_QrgGFFIn2rjHnwgwvakXU0dn00FhK9IbmE");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    // Disable the button until we have Stripe set up on the page
    document.querySelector("button").disabled = true;
    this.http.post('http://localhost/stripe-angular-back/create.php', JSON.stringify(this.purchase), httpOptions).subscribe((result: any) => {


      var elements = stripe.elements();
      var card = elements.create("card", {
        hidePostalCode: true,
      });
      // Stripe injects an iframe into the DOM
      card.mount("#card-element");
      card.on("change", function (event) {
        // Disable the Pay button if there are no card details in the Element
        //document.querySelector("button").disabled = (event.empty);
        document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
      });
      var form = document.getElementById("payment-form");
      form.addEventListener("submit", function (event) {
        event.preventDefault();
        // Complete payment when the submit button is clicked
        payWithCard(stripe, card, result.clientSecret);
      });
    });
    // Calls stripe.confirmCardPayment
    // If the card requires authentication Stripe shows a pop-up modal to
    // prompt the user to enter authentication details without leaving your page.
    var payWithCard = function (stripe, card, clientSecret) {
      loading(true);
      stripe
        .confirmCardPayment(clientSecret, {
          payment_method: {
            card: card,
            billing_details: {
              name: userCardInfo,
              email: 'alex.rigueur@gmail.com',
              phone: '0601294399',
            },
          }
        })
        .then(function (result) {
          if (result.error) {
            // Show error to your customer
            showError(result.error.message);
          } else {
            // The payment succeeded!
            orderComplete(result.paymentIntent.id);
            success();
          }
        });
    };
    /* ------- UI helpers ------- */
    // Shows a success message when the payment is complete
    var orderComplete = function (paymentIntentId) {
      loading(false);
      document
        .querySelector(".result-message a")
        .setAttribute(
          "href",
          "https://dashboard.stripe.com/test/payments/" + paymentIntentId
        );
      document.querySelector(".result-message").classList.remove("hidden");
      //document.querySelector("button").disabled = true;
    };
    // Show the customer the error from Stripe if their card fails to charge
    var showError = function (errorMsgText) {
      loading(false);
      var errorMsg = document.querySelector("#card-error");
      errorMsg.textContent = errorMsgText;
      setTimeout(function () {
        errorMsg.textContent = "";
      }, 4000);
    };
    // Show a spinner on payment submission
    var loading = function (isLoading) {
      if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("button").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
      } else {
        document.querySelector("button").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
      }
    };

    // Show success page
    var success = function () {
      document.querySelector("#form-payment").classList.add("hidden");
      document.querySelector("#success-page").classList.remove("hidden");
    };
  }

}
