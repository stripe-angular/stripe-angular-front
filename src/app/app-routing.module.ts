import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StripePaymentComponent } from './components/stripe-payment/stripe-payment.component';
import { CreateSubscriptionComponent } from './components/create-subscription/create-subscription.component';
import { SuccessSubscriptionComponent } from './components/create-subscription/success-subscription/success-subscription.component';


const routes: Routes = [
  {
    path: '',
    component: StripePaymentComponent,
  },
  {
    path: 'subscription',
    component: CreateSubscriptionComponent,
  },
  {
    path: 'success-subscription',
    component: SuccessSubscriptionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
