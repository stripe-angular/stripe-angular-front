import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StripePaymentComponent } from './components/stripe-payment/stripe-payment.component';
import { SuccessPaymentComponent } from './components/success-payment/success-payment.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateSubscriptionComponent } from './components/create-subscription/create-subscription.component';
import { SuccessSubscriptionComponent } from './components/create-subscription/success-subscription/success-subscription.component';


@NgModule({
  declarations: [
    AppComponent,
    StripePaymentComponent,
    SuccessPaymentComponent,
    CreateSubscriptionComponent,
    SuccessSubscriptionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
